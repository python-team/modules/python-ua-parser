from ua_parser import user_agent_parser
import pprint


pp = pprint.PrettyPrinter(indent=4)
ua_string = 'Apache/2.4.25 (Debian) (internal dummy connection)'
parsed_string = user_agent_parser.ParseOS(ua_string)

assert parsed_string['family'] == 'Debian'
